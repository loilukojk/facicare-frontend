import 'package:flutter/material.dart';
import 'homeScreen.dart';
import 'orderScreen.dart';
import 'notificationScreen.dart';
import 'contactScreen.dart';
import 'userScreen.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tabController = DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(title: Text("These are Tabs"), actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.person),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UserScreen()),
                );
              },
            )
          ]),
          body: TabBarView(children: [
            HomeScreen(),
            OrderScreen(),
            NotificationScreen(),
            ContactScreen()
          ]),
          bottomNavigationBar: TabBar(
            tabs: [
              Tab(
                icon: new Icon(Icons.home),
                text: "Trang chủ",
              ),
              Tab(
                icon: new Icon(Icons.library_books),
                text: "Đơn hàng",
              ),
              Tab(
                icon: new Icon(Icons.notifications),
                text: "Thông báo",
              ),
              Tab(
                icon: new Icon(Icons.phone),
                text: "Liên hệ",
              )
            ],
            labelColor: Color(0xFF527DAA),
            unselectedLabelColor: Color(0xFF636E72),
            indicatorSize: TabBarIndicatorSize.label,
            labelStyle: TextStyle(fontSize: 10.0, fontFamily: "Roboto"),
          ),
        ));

    return tabController;
  }
}

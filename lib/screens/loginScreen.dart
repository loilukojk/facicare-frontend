import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jwt_decode/jwt_decode.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:facicareapp/global.dart';
import '../utilities/loginStyle.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  bool _rememberMe = false;
  String username = "";
  String password = "";

  Widget _usernameTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Username",
          style: loginLabelStyle,
        ),
        Container(
          margin: EdgeInsets.only(top: 11.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              color: Color(0xFF61A4F1),
              boxShadow: [loginTFBoxShadow]),
          child: TextField(
              decoration: InputDecoration(
                  hintText: 'Enter your username',
                  hintStyle: loginHintStyle,
                  contentPadding: EdgeInsets.only(top: 14.0),
                  border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.person,
                    color: Colors.white,
                  )),
              style: loginTextInput,
              keyboardType: TextInputType.text,
              autocorrect: false,
              onChanged: (text) {
                this.username = text;
              }),
        )
      ],
    );
  }

  Widget _passwordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Password",
          style: loginLabelStyle,
        ),
        Container(
          margin: EdgeInsets.only(top: 11.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              color: Color(0xFF61A4F1),
              boxShadow: [loginTFBoxShadow]),
          child: TextField(
              decoration: InputDecoration(
                  hintText: 'Enter your password',
                  hintStyle: loginHintStyle,
                  contentPadding: EdgeInsets.only(top: 14.0),
                  border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.white,
                  )),
              style: loginTextInput,
              keyboardType: TextInputType.text,
              autocorrect: false,
              obscureText: true,
              //secure your text entry, display ****
              onChanged: (text) {
                this.password = text;
              }),
        )
      ],
    );
  }

  Widget _forgotPasswordLB() {
    return Container(
      alignment: Alignment.centerRight,
      margin: EdgeInsets.only(top: 31),
      child: Text(
        "Forgot Password?",
        style: loginLabelStyle,
      ),
    );
  }

  Widget _rememberCB() {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
                value: _rememberMe,
                checkColor: Colors.green,
                activeColor: Colors.white,
                onChanged: (value) {
                  setState(() {
                    _rememberMe = value;
                  });
                }),
          ),
          Text(
            'Remember me',
            style: loginLabelStyle,
          ),
        ],
      ),
    );
  }

  Widget _loginBTN(GlobalKey<ScaffoldState> _scaffoldKey) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: 15.0),
            height: 54.0,
            child: RaisedButton(
                child: Text(
                  "LOGIN",
                  style: TextStyle(
                      fontSize: 22.0,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.5,
                      color: Color(0xFF527DAA)),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0)),
                onPressed: () async {
                  var url = URL_LOGIN;
                  Map<String, String> requestHeaders = {
                    "Content-type": "application/json"
                  };
                  String requestBody =
                      '{"username": "$username", "password": "$password"}';
                  var response = await http.post(url,
                      headers: requestHeaders, body: requestBody);
                  final resBody = json.decode(response.body);
                  print('Response status: ${response.statusCode}');
                  print('Response body: ${response.body}');
                  if (resBody["token"] != null) {
                    final token = resBody["token"];
                    var payload = Jwt.parseJwt(token);
                    print(payload);
                    // redirect to another page
                    Navigator.pushReplacementNamed(context, "/home");
                  } else {
                    print("Token is null - Login failed");
                    final snackBar = new SnackBar(
                      content: new Text("Đăng nhập thất bại"),
                      duration: new Duration(seconds: 2),
                      backgroundColor: Colors.orange,
                    );
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }
                },
                color: Colors.white),
          ),
        )
      ],
    );
  }

  Widget _dontHaveAnAccountLB() {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Don't have an Account?",
            style: loginLabelStyle,
          ),
          InkWell(
            child: Text(
              " Sign up",
              style: loginSignupLabelStyle,
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, "/signup");
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: Container(
        padding: EdgeInsets.only(left: 37.0, right: 37.0),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Sign in",
                style: TextStyle(
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w700,
                  fontSize: 40.0,
                  color: Color(0xFFFFFFFF),
                )),
            SizedBox(height: 31),
            _usernameTF(),
            SizedBox(height: 30),
            _passwordTF(),
            _forgotPasswordLB(),
            _rememberCB(),
            _loginBTN(_scaffoldKey),
            _dontHaveAnAccountLB()
          ],
        ),
      ),
    );
  }
}

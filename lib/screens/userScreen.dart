import 'package:flutter/material.dart';

class UserScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Thông tin người dùng"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Tên: Khách hàng A"),
            Text("Địa chỉ: Nhà ở Kim Biên"),
            FlatButton(
              child: Text("Log out"),
              color: Colors.blueAccent,
              textColor: Colors.white,
              onPressed: () {
                Navigator.pushReplacementNamed(context, "/login");
              },
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:facicareapp/global.dart';
import '../utilities/loginStyle.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignUpScreenState();
  }
}

class SignUpScreenState extends State<SignUpScreen> {
  String phoneNumber = "";
  String password = "";
  String retypePassword = "";
  String fullName = "";
  String email = "";
  String address = "";

  Widget _phoneNumberTF() {
    return Container(
      margin: EdgeInsets.only(top: 11.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Color(0xFF61A4F1),
          boxShadow: [loginTFBoxShadow]),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Phone number',
              hintStyle: loginHintStyle,
              contentPadding: EdgeInsets.only(top: 14.0),
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.phone,
                color: Colors.white,
              )),
          style: loginTextInput,
          keyboardType: TextInputType.text,
          autocorrect: false,
          onChanged: (text) {
            this.phoneNumber = text;
          }),
    );
  }

  Widget _passwordTF() {
    return Container(
      margin: EdgeInsets.only(top: 11.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Color(0xFF61A4F1),
          boxShadow: [loginTFBoxShadow]),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Enter your password',
              hintStyle: loginHintStyle,
              contentPadding: EdgeInsets.only(top: 14.0),
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              )),
          style: loginTextInput,
          keyboardType: TextInputType.text,
          autocorrect: false,
          obscureText: true,
          //secure your text entry, display ****
          onChanged: (text) {
            this.password = text;
          }),
    );
  }

  Widget _retypePasswordTF() {
    return Container(
      margin: EdgeInsets.only(top: 11.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Color(0xFF61A4F1),
          boxShadow: [loginTFBoxShadow]),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Enter your password',
              hintStyle: loginHintStyle,
              contentPadding: EdgeInsets.only(top: 14.0),
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              )),
          style: loginTextInput,
          keyboardType: TextInputType.text,
          autocorrect: false,
          obscureText: true,
          //secure your text entry, display ****
          onChanged: (text) {
            this.retypePassword = text;
          }),
    );
  }

  Widget _fullNameTF() {
    return Container(
      margin: EdgeInsets.only(top: 11.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Color(0xFF61A4F1),
          boxShadow: [loginTFBoxShadow]),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Full name',
              hintStyle: loginHintStyle,
              contentPadding: EdgeInsets.only(top: 14.0),
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.person,
                color: Colors.white,
              )),
          style: loginTextInput,
          keyboardType: TextInputType.text,
          autocorrect: false,
          onChanged: (text) {
            this.fullName = text;
          }),
    );
  }

  Widget _emailTF() {
    return Container(
      margin: EdgeInsets.only(top: 11.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Color(0xFF61A4F1),
          boxShadow: [loginTFBoxShadow]),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Email Address',
              hintStyle: loginHintStyle,
              contentPadding: EdgeInsets.only(top: 14.0),
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.email,
                color: Colors.white,
              )),
          style: loginTextInput,
          keyboardType: TextInputType.emailAddress,
          autocorrect: false,
          onChanged: (text) {
            this.email = text;
          }),
    );
  }

  Widget _addressTF() {
    return Container(
      margin: EdgeInsets.only(top: 11.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          color: Color(0xFF61A4F1),
          boxShadow: [loginTFBoxShadow]),
      child: TextField(
          decoration: InputDecoration(
              hintText: 'Address',
              hintStyle: loginHintStyle,
              contentPadding: EdgeInsets.only(top: 14.0),
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.location_on,
                color: Colors.white,
              )),
          style: loginTextInput,
          keyboardType: TextInputType.text,
          autocorrect: false,
          onChanged: (text) {
            this.address = text;
          }),
    );
  }

  Widget _signUpBTN(GlobalKey<ScaffoldState> _scaffoldKey) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: 15.0),
            height: 54.0,
            child: RaisedButton(
                child: Text(
                  "SIGN UP",
                  style: TextStyle(
                      fontSize: 22.0,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.5,
                      color: Color(0xFF527DAA)),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0)),
                onPressed: () async {
                  var url = URL_SIGNUP;
                  Map<String, String> requestHeaders = {
                    "Content-type": "application/json"
                  };
                  String requestBody =
                      '{"username":"$phoneNumber","password":"$password","roles":"","first_name":"$fullName","last_name":"","phone_number":"$phoneNumber","address":"$address","email":"$email"}';
                  print("Body: $requestBody");
                  var response = await http.post(url,
                      headers: requestHeaders, body: requestBody);
                  final resBody = json.decode(response.body);
                  print('Response status: ${response.statusCode}');
                  print('Response body: ${response.body}');
                  if (resBody["code"] == '0') {
                    print("Signup is success, msg: ${resBody['message']}");
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Đăng ký thành công"),
                            content: Text("Di chuyển đến trang đăng nhập"),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("Sign In"),
                                onPressed: () {
                                  Navigator.pushReplacementNamed(
                                      context, "/login");
                                },
                              )
                            ],
                          );
                        });
                  } else {
                    print("Signup failed");
                    final snackBar = new SnackBar(
                      content: new Text("Đăng ký thất bại"),
                      duration: new Duration(seconds: 2),
                      backgroundColor: Colors.orange,
                    );
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }
                },
                color: Colors.white),
          ),
        )
      ],
    );
  }

  Widget _haveAnAccountLB() {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Already have an Account?",
            style: loginLabelStyle,
          ),
          InkWell(
            child: Text(
              " Sign in",
              style: loginSignupLabelStyle,
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, "/login");
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: Container(
        padding: EdgeInsets.only(left: 37.0, right: 37.0),
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF73AEF5),
              Color(0xFF61A4F1),
              Color(0xFF478DE0),
              Color(0xFF398AE5),
            ],
            stops: [0.1, 0.4, 0.7, 0.9],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Sign up",
                style: TextStyle(
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w700,
                  fontSize: 40.0,
                  color: Color(0xFFFFFFFF),
                )),
            _phoneNumberTF(),
            _passwordTF(),
            _retypePasswordTF(),
            _fullNameTF(),
            _emailTF(),
            _addressTF(),
            _signUpBTN(_scaffoldKey),
            _haveAnAccountLB()
          ],
        ),
      ),
    );
  }
}

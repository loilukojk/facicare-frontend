class User {
  int exp;
  UserInfo information;
  String role;
}

class UserInfo {
  String address;
  String email;
  String fullName;    // first_name
  String username;
  String password;
  String phoneNumber; // phone_number
  String roles;
  String userID;      // user_id
}

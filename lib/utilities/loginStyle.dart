import 'package:flutter/material.dart';

final loginLabelStyle =
    TextStyle(fontFamily: "Roboto", fontSize: 18.0, color: Color(0xFFFFFFFF));

final loginTFBoxShadow = BoxShadow(
  color: Color.fromARGB(25, 0, 0, 0),
  offset: Offset(0, 4),
  blurRadius: 4,
);

final loginHintStyle =
    TextStyle(color: Color(0xFFD8D9DA), fontFamily: "Roboto", fontSize: 18.0);

final loginTextInput =
    TextStyle(color: Color(0xFFFFFFFF), fontSize: 18.0, fontFamily: "Roboto");

final loginSignupLabelStyle = TextStyle(
    color: Color(0xFFFFFFFF),
    fontSize: 18.0,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w700);

import 'package:facicareapp/screens/mainScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'screens/loginScreen.dart';
import 'screens/signUpScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'FaciCare',
        debugShowCheckedModeBanner: false,
        home: LoginScreen(),
        routes: {
          "/login": (_) => LoginScreen(),
          "/signup": (_) => SignUpScreen(),
          "/home": (_) => MainScreen(),
        });
  }
}
